from fastapi.security import HTTPBearer, HTTPAuthorizationCredentials
from fastapi import Depends, HTTPException
from .db import user_table, execute
from sqlalchemy import select

auth_scheme = HTTPBearer()

def get_user(token:HTTPAuthorizationCredentials = Depends(auth_scheme)):
    user = execute(select(user_table).where(user_table.c.session_token == token.credentials)).one_or_none()
    if not user:
        raise HTTPException(status_code=401, detail="session token invalid")
    return user

