import os
from sqlalchemy import create_engine, MetaData
from sqlalchemy import Table, Column, Integer, String, DateTime

db_url = os.getenv("DATABASE_URL")
if not db_url:  # test environment
    db_url = "sqlite+pysqlite:///:memory:"
if db_url.startswith("postgres://"):
    db_url = db_url.replace("postgres://", "postgresql://", 1)
engine = create_engine(db_url, future=True)

metadata_obj = MetaData()

maze_table = Table(
    "maze", metadata_obj,
    Column('id', Integer, primary_key=True),
    Column('owner', Integer),
    Column('n_rows', Integer),
    Column('n_columns', Integer),
    Column('walls', String),  # json
    Column('entrance_row', Integer),
    Column('entrance_column', Integer),
    Column('min_solution_status', String),
    Column('min_solution', String, nullable=True),  # json
    Column('max_solution_status', String),
    Column('max_solution', String, nullable=True)  # json
)

user_table = Table(
    "user_account", metadata_obj,
    Column('id', Integer, primary_key=True),
    Column('username', String),
    Column('password', String),  # todo: make this hashed
    Column('last_login', DateTime),
    Column('session_token', String)
)

metadata_obj.create_all(engine)


def execute(query):
    """convenience function to execute queries"""
    with engine.begin() as connection:
        return connection.execute(query)
