from fastapi import APIRouter, Depends
from pydantic import Field
from sqlalchemy import select
import string, json

router = APIRouter()

from ..db import maze_table, execute
from ..authentication import get_user
from ..schemata import NewMaze, SolutionStatus, CellSet

class SolvedMaze(NewMaze):
    minSolutionStatus:SolutionStatus = Field(..., example='solved')
    minSolution:CellSet = Field(..., example=["A0", "B0", "B1", "B2", "A2", "A3", "A4", "A5", "A6", "A7"])
    maxSolutionStatus:SolutionStatus = Field(..., example='pending')
    maxSolution:CellSet = Field(..., example=[])


@router.get("/maze", response_model=list[SolvedMaze])
def get_mazes(user = Depends(get_user)):
    """Shows all the mazes that have been created by the logged in user"""
    query = select(maze_table).where(maze_table.c.owner == user.id)
    mazes = execute(query).all()
    mazes = _render_get_mazes_result(mazes)
    return mazes

def _render_get_mazes_result(mazes):
    _to_cellformat = lambda c, r: string.ascii_uppercase[c] + str(r)
    _convert_solution = lambda str: [_to_cellformat(c,r) for c,r in json.loads(str or "[]")]
    return [{
            'id': maze.id,
            'gridSize': f"{maze.n_rows}x{maze.n_columns}",
            'walls': [_to_cellformat(*wall) for wall in json.loads(maze.walls)],
            'entrance': _to_cellformat(maze.entrance_column, maze.entrance_row),
            'minSolutionStatus': maze.min_solution_status,
            'minSolution': _convert_solution(maze.min_solution),
            'maxSolutionStatus': maze.max_solution_status,
            'maxSolution': _convert_solution(maze.max_solution),
        } for maze in mazes]


# todo: tests