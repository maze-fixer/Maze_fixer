from sqlalchemy import insert, select
from fastapi import HTTPException, APIRouter

from ..schemata import UserIn
from ..db import user_table, execute

router = APIRouter()

@router.post("/user", response_model=int, tags=["Authentication"])
def register(user:UserIn):
    """Creates a new user with the given parameters"""
    query = get_user_query(user)
    get_user_result = execute(query)
    if get_user_result.one_or_none():
        raise HTTPException(status_code=409, detail=f"a user named '{user.username}' already exists")
    query = add_user_query(user)
    userid = execute(query).inserted_primary_key[0]
    return userid

def get_user_query(user):
    return select(user_table).where(user_table.c.username == user.username)

def add_user_query(user):
    return insert(user_table).values(
        username = user.username,
        password = user.password
    )

# todo: tests